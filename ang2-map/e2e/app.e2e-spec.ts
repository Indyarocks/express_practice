import { Ang2MapPage } from './app.po';

describe('ang2-map App', function() {
  let page: Ang2MapPage;

  beforeEach(() => {
    page = new Ang2MapPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
