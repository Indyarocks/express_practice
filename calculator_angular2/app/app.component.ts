import { Component } from '@angular/core';

@Component({
   selector: 'my-app',
   templateUrl: 'app/calculator.html'
})


export class AppComponent {
	number1=0;
	number2=0;
	result=0;
	operator = "";
	performOperation(operation){
		console.log(this.number1+" "+operation+" "+this.number2);
		this.operator = operation;
		switch(operation){
			case '+': this.result = this.number1 + this.number2;
						break;
			case '-': this.result = this.number1 -  this.number2;;
						break;
			case '*': this.result = this.number1 * this.number2;;
						break;
			case '/': this.result = this.number1 / this.number2;;
						break;
			default: this.result = this.number1 +  this.number2;;												
		}
		
	}
}
