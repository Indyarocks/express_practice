var express = require('express');
var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();
app.use('/api', router);

var path = require('path');
app.get('/', function (req, res) {
    res.sendFile(path.resolve('index.html'));
});

router.post('/addition', function (req, res) {
    var result = parseInt(req.body.number1, 10) + parseInt(req.body.number2, 10);
    
    res.json({ Result: result });   
});

router.post('/substraction', function (req, res) {
    var result = parseInt(req.body.number1, 10) - parseInt(req.body.number2, 10);
    
    res.json({ Result: result });   
});

router.post('/multiplication', function (req, res) {
    var result = parseInt(req.body.number1, 10) * parseInt(req.body.number2, 10);
    
    res.json({ Result: result });   
});

router.post('/division', function (req, res) {
    var result = parseInt(req.body.number1, 10) / parseInt(req.body.number2, 10);
    
    res.json({ Result: result });   
});

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});


var server = app.listen(5000, function () {
    console.log('Node server is running.. port 5000');
});