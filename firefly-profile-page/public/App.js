import React, { Component } from 'react';
import Popover from 'react-popover-control';
import './App.css';
require('react-datepicker/dist/react-datepicker.css');


class App extends Component {
  constructor(props){
    super(props)
    this.state= {
        open: false
    }
  }
  _handleDropDown(){
    this.setState({
      open:!this.state.open
    })
  }

  render() {
    return (
      // <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
      <div className="App">
    <div className="row header">
        <div className="col-md-8 logo-container">
            <div className="logo"></div>
        </div>
        <div className="col-md-3 search text-right">
            <div className="searchCont">
                <i className="fa fa-search"></i>
                <input type="text" placeholder="Search" />
            </div>
        </div>
        <div className="col-md-1 profile-pc">
          <div className="dropdown">
            <div id="header-profile" className="pull-left dropdown-toggle" data-toggle="dropdown"></div>
            <ul className="dropdown-menu in-header">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Logout</a></li>
            </ul>
          </div>
        </div>
    </div>

    <div className="App-logo"></div>

    <div className="dropdown-lists">
        <a href="#">
            <span id="image-icon" onClick={this._handleDropDown.bind(this)}>

                  <div className="dropdown">
                      <span className="btn dropdown-toggle cover-pic" data-toggle="dropdown">
                        </span>
            <ul className="dropdown-menu coverPicMenu">
                <li><a href="#">Upload</a></li>
                <li><a href="#">Remove</a></li>
                <li><a href="#">cancel</a></li>
            </ul>
    </div>
    </span>
    </a>
</div>

<form>
    <div className="row body-containt">
        <div className="col-md-2 ">
            <div className="profile-pic profile-pic-bcgnd">

                <div className="dropdown">
                    <span className="btn dropdown-toggle profile-dw" data-toggle="dropdown">
                        </span>
                    <ul className="dropdown-menu">
                        <li><a href="#">Upload</a></li>
                        <li><a href="#">Remove</a></li>
                        <li><a href="#">cancel</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="col-md-3">
            <input type="text" className="textfild" name="email" placeholder="Email" />
            <br/>
            <span className="pull-left">@gmail.com</span>
            <br/>
            <input type="text" className="textfild" name="url" placeholder="url" />
            <br/>
            <input type="date" className="textfild" name="date" placeholder="Birth date" />
            <br/>

            <input type="textarea" className="textfild" name="msg" placeholder="About your self" />
            <br/>

        </div>
        <div className="col-md-7"></div>
    </div>
</form>
<div className="row">
    <div className="col-md-2 likes">
        <span id="likes">0</span>
        <br/>
        <a id="static-likes">Likes</a>
    </div>
    <div className="col-md-10 comments">
        <span id="comments">0</span>
        <br/>
        <a id="static-comments">Comments</a>
    </div>
</div>
<div className="row btnBlock">
    <div className="col-md-2 btn-cancel">
        <button type="button" className="btn btn-default" onClick={this.handleLogin}>Cancel</button>
    </div>
    <div className="col-md-10 btn-save">
        <button type="button" className="btn btn-primary" onClick={this.handleLogin}>Save Changes</button>
    </div>
</div>
</div>
); }
}

export default App;
