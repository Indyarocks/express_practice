import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    public fName: string =  'pradip';
    public lName: string = 'joshi';
    public reservationMade: string = '2016-06-22T07:18-08:00'
    public reservationFor: string = "2025-11-14";
    public cost: number = 99.99;
}
